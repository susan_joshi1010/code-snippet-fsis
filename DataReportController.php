<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class DataReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.data_report.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function showReport(Request $request)
    {
        $interval=$request['interval'];
        $filter=$request['filter'];
        
        $district_id=[];
        $first_aider_id=[];
        $first_responder_id=[];
        $gender_id=[];

        $age_from='';
        $age_till='';
        
        $case_id=[];
        $area=[];

        $sub_qry='';
        // dd($filter);
        foreach ($filter as $key => $value) {
            $filter='';
            //run only if any filter has been added
            if (count($value)>0) {
                foreach ($value as $k => $v) {
                    // dd($v['status']);
                    switch ($v['status']) {
                        case 1:
                            array_push($district_id,$v['id']);
                            break;
                        case 2:
                            array_push($first_aider_id,$v['id']);
                            break;
                        case 3:
                            array_push($first_responder_id,$v['id']);
                            break;
                        case 4:
                            array_push($gender_id,$v['id'].'%');
                            break;
                        
                        case 6:
                            array_push($case_id,$v['id']);
                            break;
                        case 7:
                            array_push($area,$v['name'].'%');
                            break;
                        default:
                            break;
                    }
                    if($v['status']==5){
                        if($v['age_type_from']=='M') {
                            $age_from = $v['age_from'];
                        }else{
                            $age_from = $v['age_from']*12;
                        }
                        if($v['age_type_till']=='M') {
                            $age_till = $v['age_till'];
                        }else{
                            $age_till = $v['age_till']*12;
                        }
                        $sub_qry="AND case when p.age_type='Y' THEN p.age*12 ELSE p.age END BETWEEN $age_from AND $age_till ";
                    }
                }
            }
        }

        if (count($district_id)>0) {
            $sub_qry.="AND d.id in (".implode(',',$district_id).") ";
        }
        if (count($first_aider_id)>0) {
            $sub_qry.="AND fa.first_aider_id in (".implode(',',$first_aider_id).") ";
        }
        if (count($first_responder_id)>0) {
            $sub_qry.="AND fr.first_responder_id in (".implode(',',$first_responder_id).") ";
        }
        if (count($gender_id)>0) {
            $sub_qry.="AND p.gender like ";
            $end_key='';
            end($gender_id);         
            $end_key = key($gender_id);
            foreach ($gender_id as $key => $value) {
                if ($end_key!=$key) {
                    $sub_qry.="'$value' OR ";
                }else{
                    $sub_qry.="'$value' ";
                }
            }
        }
        if (count($case_id)>0) {
            $sub_qry.="AND c.id in (".implode(',',$case_id).") ";
        }
        if (count($area)>0) {
            $sub_qry.="AND p.area like ";
            $end_key='';
            end($area);
            foreach ($area as $key => $value) {
                if ($end_key!=$key) {
                    $sub_qry.="'$value' OR ";
                }else{
                    $sub_qry.="'$value' ";
                }
            }
        }
        $sub_qry.=' And p.deleted_at is NULL';
        try{
            $reports=DB::select("SELECT p.name patient_name, 
                                      date(r.created_at) as reported_date,
                                      u.full_name reported_by,
                                      d.district_name,
                                      d.id as district_id,
                                      r.id as report_id, 
                                      fr.first_responder_id, 
                                      fr.first_responder_type,
                                      fa.first_aider_type, 
                                      fa.first_aider_id,
                                      p.gender,
                                      concat(p.age,p.age_type) age, 
                                      c.name case_name,
                                      c.id case_id,
                                      p.area
                                FROM reports r
                                    INNER JOIN patients p on p.id=r.patient_id
                                    INNER JOIN cases c on r.case_id=c.id
                                    INNER JOIN districts d on p.district_id=d.id
                                    INNER JOIN first_aid_reports far on far.report_id=r.id
                                    INNER JOIN firstaids f on f.id=far.first_aid_detail_id
                                    INNER JOIN users u on u.id=r.created_by
                                    INNER JOIN first_responders fr on fr.first_responder_id=u.first_responder
                                    INNER JOIN first_aiders fa on fa.first_aider_id=u.first_aider
                                 WHERE date(r.created_at) between '".$interval['from']."'  AND '".$interval['to']."'
                                 $sub_qry
                                GROUP BY p.name,r.id,c.name,r.created_at
                                order by r.id desc
                                ;");
            return response()->json(['reports'=>$reports,'status'=>1]);
        }catch(\Illuminate\Database\QueryException $ex){
            return response()->json(['message'=>$ex->errorInfo['2'],'status'=>0]);
        }

    }
}
